/**
 * Task 2: write a method that will give me the total number of letters 
 * in all the names with more than 5 letters 'by using Stream'
 */
import java.util.stream.Stream;

public class Task2 {
    public static Integer returnCount(String... names) {
        return Stream.of(names)
                .filter(f -> f.length() > 5)
                .map(m -> m.length())
                .reduce(0, Integer::sum);

    }

    public static void main(String[] args) {
        System.out.println(Task2.returnCount("a", "ab", "abc", "abcd", "abcde", "abcdef", "abcdefg", "abcdefgh"));
    }
}



/**
 * Task 2: write a method that will give me the total number of letters 
 * in all the names with more than 5 letters 'by using Stream'
 */
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task2 {
    public static List<Integer> returnCount(String... names) {
        return Stream.of(names)
                .filter(f -> f.length() > 5)
                .map(m -> m.length())
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        System.out.println(Task2.returnCount("a", "ab", "abc", "abcd", "abcde", "abcdef", "abcdefg", "abcdefgh"));
    }
}
